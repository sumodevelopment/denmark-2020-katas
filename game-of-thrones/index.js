export const ALWAYS_LOWERCASE = [
    'and',
    'the',
    'of',
    'in'
]

export function isAlwaysLowerCase(word) {
    return ALWAYS_LOWERCASE.includes(word.toLowerCase())
}

export function formatWord(word) {
    if (isAlwaysLowerCase(word)) {
        return word.toLowerCase()
    }

    return word[0].toUpperCase() + word.slice(1).toLowerCase()
}

export function addPeriod(title) {
    if (title[title.length - 1] !== ".") {
        return `${title}.`
    }
    return title
}

export function spaceCommas(title) {
    const commaMatch = title.match(/\,/)

    if (!commaMatch) {
        return title
    }

    const commaIndex = commaMatch.index

    if (title[commaIndex + 1] !== ' ') {
        title = title.replace(/\,/g, ', ')
    }

    return title
}

export function correctTitle(title) {
    let formattedTitle = spaceCommas(title)
    formattedTitle = addPeriod(formattedTitle)
    formattedTitle = formattedTitle.split(' ').map(formatWord).join(' ')
    return formattedTitle
}

