import { caesarCipher, newCipher, isNotAlpha, isUpperCase } from './'

test('Ceasers Cipher Shortest', ()=> {
    const source = "middle-Outz"
    const output = "okffng-Qwvb"
    expect( caesarCipher(source, 2) ).toBe( output )
})

describe("Is special character", () => {
    test('! >> true', ()=> {
        expect(  isNotAlpha("!") ).toBe( true )
    })
    test('@ >> true', ()=> {
        expect(  isNotAlpha("@") ).toBe( true )
    })
    test('- >> true', ()=> {
        expect(  isNotAlpha("-") ).toBe( true )
    })    
    test('a >> false', ()=> {
        expect(  isNotAlpha("a") ).toBe( false )
    })
    test('z >> false', ()=> {
        expect(  isNotAlpha("z") ).toBe( false )
    })
    test('2 >> true', ()=> {
        expect(  isNotAlpha("2") ).toBe( true )
    })
})

describe('Is uppercase character', () => {
    test('A >> true', ()=> {
        expect(  isUpperCase("A") ).toBe( true )
    })
    test('a >> false', ()=> {
        expect(  isUpperCase("a") ).toBe( false )
    })
    test('Z >> true', ()=> {
        expect(  isUpperCase("Z") ).toBe( true )
    })
    test('! >> false', ()=> {
        expect(  isUpperCase("!") ).toBe( false )
    })
})

describe("Ceasers Cipher", () => {
    test('middle-Outz >> okffng-Qwvb', ()=> {
        const source = "middle-Outz"
        const output = "okffng-Qwvb"
        expect( newCipher(source, 2) ).toBe( output )
    })
})

