import { isBoomerang, numberOfBoomerangs } from './'

describe('is boomerang', ()=> {
    test('[3, 7, 3] is boomerang to be true', ()=>{
        const source =  [3, 7, 3]
        expect( isBoomerang( source ) ).toBe( true )
    })
    
    test('[3, 3, 3] is boomerang to be false', ()=>{
        const source =  [3, 3, 3]
        expect( isBoomerang( source ) ).toBe( false )
    })
    
    test('[3, -4, 3] is boomerang to be true', ()=>{
        const source =  [3, -4, 3]
        expect( isBoomerang( source ) ).toBe( true )
    })

    test('[9, 2] is boomerang to be false', ()=>{
        const source =  [9,2]
        expect( isBoomerang( source ) ).toBe( false )
    })
})



describe('number of boomerangs', ()=> {
    test('split into 3 sets', ()=> {
        const source =  [3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2]
        expect( numberOfBoomerangs( source ) ).toBe( 3 )
    })

    test('split into 4 sets', ()=> {
        const source =  [2, 1, 2, 1, 2, -2, 2, 7]
        expect( numberOfBoomerangs( source ) ).toBe( 4 )
    })

    test('split into 1 sets', ()=> {
        const source =  [4, 5, 4]
        expect( numberOfBoomerangs( source ) ).toBe( 1 )
    })

    test('split into 0 sets', ()=> {
        const source =  [4, 5]
        expect( numberOfBoomerangs( source ) ).toBe( 0 )
    })
})
