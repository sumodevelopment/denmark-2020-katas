import { split, shuffle, matches, faro } from './'

// UNIT TESTS
describe( 'UNIT: Evenly split arrays', ()=> {
    test('split [1, 2, 3, 4] evenly to [1,2] - [3,4]', () => {
        const source = [1, 2, 3, 4]
        const expected = [
            [1,2],
            [3,4]
        ]
        expect( split( source ) ).toEqual( expected )
    })

    test('split [1, 2, 3, 4, 5, 6, 7, 8] evenly', () => {
        const source = [1, 2, 3, 4, 5, 6, 7, 8]
        const expected = [
            [1,2,3,4],
            [5,6,7,8]
        ]
        expect( split( source ) ).toEqual( expected )
    })
})

describe('UNIT: Shuffle cards', () => {

    test('shuffle [1,2] [3,4] to [1,3,2,4]', ()=> {
        const sourceLeft = [ 1,2 ]
        const sourceRight = [ 3,4 ]
        const expected = [1,3,2,4]
        expect( shuffle( sourceLeft, sourceRight ) )
            .toEqual( expected )
    })

    test('shuffle [1,2,3,4] [5,6,7,8] to [1,5,2,6,3,7,4,8]', ()=> {
        const sourceLeft = [ 1,2,3,4 ]
        const sourceRight = [ 5,6,7,8 ]
        const expected = [1,5,2,6,3,7,4,8]
        expect( shuffle( sourceLeft, sourceRight ) )
            .toEqual( expected )
    })

})

describe('UNIT: Array comparison', ()=>{

    test('[1,2,3,4] == [1,2,3,4] should be true', ()=> {
        const origin = [1,2,3,4]
        const shuffled = [1,2,3,4]
        expect( matches( origin, shuffled ) ).toBe( true )
    })

    test('[5,2,1,4] == [9,1,9,2] should be false', ()=> {
        const origin = [5,2,1,4]
        const shuffled = [9,1,9,2]
        expect( matches( origin, shuffled ) ).toBe( false )
    })

})

// INTEGRATION TESTING
describe( 'INT: Faro Shuffle', ()=> {

    test('[1,2,3,4] Should shuffle 2 times', () => {
        const deck = [1,2,3,4]
        expect( faro( deck ) ).toBe( 2 )
    })

    test('[1,2,3,4,5,6,7,8] Should shuffle 3 times', () => {
        const deck = [1,2,3,4,5,6,7,8]
        expect( faro( deck ) ).toBe( 3 )
    })

})