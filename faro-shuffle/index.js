export function split(arrayToSplit) {
    return [
        arrayToSplit.slice(0, arrayToSplit.length/2),
        arrayToSplit.slice(arrayToSplit.length/2),
    ]
}

export function shuffle( leftDeck, rightDeck ) {
    const shuffled = []
    for (let i = 0; i < leftDeck.length; i ++) {
        shuffled.push( leftDeck[i] )
        shuffled.push( rightDeck[i] )
    }
    return shuffled
}

export function matches( origin, shuffled ) {
    return JSON.stringify( origin ) === JSON.stringify( shuffled )
}

export function faro(deck) {
    let shuffled = [...deck]
    let shuffleCount = 0
    do {
        const [ left, right ] = split( shuffled )
        shuffled = shuffle( left, right )
        shuffleCount++
    } while( matches( deck, shuffled ) === false )
    return shuffleCount
}