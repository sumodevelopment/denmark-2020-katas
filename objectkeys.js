function removeSpecialCharacters(string) {
    return string.replace(/[^a-zA-Z' ]/g, '')
}

const string = "See Spot' run, See Spot jump. Spot likes jumping. See Spot fly."
const cleanString = removeSpecialCharacters( string )

const words = cleanString.split(' ')
const wordCount = {}

for (let i = 0; i < words.length; i++) {
    const key = words[i]
    if (wordCount[key]) {
        wordCount[key] += 1
    }
    else {
        wordCount[key] = 1
    }
}

console.log( wordCount['Spot'] )