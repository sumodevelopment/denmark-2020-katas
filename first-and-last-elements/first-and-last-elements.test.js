const { test, expect } = require("@jest/globals");

import { firstAndLast } from './first-and-last-elements';

test('return test and 5', () => {
    const source = ['test', 4, 5]
    expect( firstAndLast( source ) ).toEqual(['test', 5]);
})

test('return []', () => {
    const source = []
    expect( firstAndLast( source ) ).toEqual([]);
})

test('return undefined and hello', () => {
    const source = [undefined, 'nine', false, 'hello']
    expect( firstAndLast( source ) ).toEqual([ undefined, 'hello' ]);
})

test('return -1 and wild!', () => {
    const source = [-1, false, false, 103, -123, 5559, 'wild!']
    expect( firstAndLast( source ) ).toEqual([ -1, 'wild!' ]);
})

test('return [] when given null', () => {
    const source = null
    expect( firstAndLast( source ) ).toEqual([ ]);
})

test('return {} and 9', () => {
    const source = [{}, 'javascript', false, undefined, 9]
    expect( firstAndLast( source ) ).toEqual([{}, 9]);
})

test('return with 1 item in array', () => {
    const source = ['test']
    expect( firstAndLast( source ) ).toEqual(['test', 'test']);
})

test('multiple depth arrays', () => {
    const source = [
        [true, false],
        ["Hi", "State"]  
        [ 3, 4 ], 
        "hi", 
        [ [undefined], [null] ],
        [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
    ]

    expect( firstAndLast( source ) ).toEqual([[true, false], [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]]);
})

