export function firstAndLast(args) {
    if (args === null || args.length === 0) return []
    return [
        args[0],
        args[args.length - 1]
    ]
}
