export function existsHigher(values, highest) {
    const highestInArray = Math.max(...values)
    return highestInArray >= highest
}