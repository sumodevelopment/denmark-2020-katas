import { existsHigher } from './'

test('return false for [1, 2, 3, 4] -> 10', () => {
    const source = [ 1, 2, 3, 4 ]
    const highest = 10
    expect( existsHigher(source, highest) ).toBe( false )
})

test('return false for [-515, -52, -33, -24] -> -1', () => {
    const source = [-515, -52, -33, -24]
    const highest = -1
    expect( existsHigher(source, highest) ).toBe( false )
})

test('return true for [1] -> 1', () => {
    const source = [ 1 ]
    const highest = 1
    expect( existsHigher(source, highest) ).toBe( true )
})

test('return true for [-1] -> -1', () => {
    const source = [ -1 ]
    const highest = -1
    expect( existsHigher(source, highest) ).toBe( true )
})

test('return true for [30, 30] -> 30', () => {
    const source = [ 30, 30 ]
    const highest = 30
    expect( existsHigher(source, highest) ).toBe( true )
})