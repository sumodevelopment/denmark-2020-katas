import { hasNumbers, numInStr } from './'

describe('Check for Numbers in Strings', () => {
    
    test('Hl24lk to be true', ()=> {
        const source = 'Hl24lk'
        expect( hasNumbers( source )).toBe( true )
    })

    test('I Love JavaScript! to be false', ()=> {
        const source = 'I Love JavaScript!'
        expect( hasNumbers( source )).toBe( false )
    })
    
    test('135 to be true', ()=> {
        const source = '135'
        expect( hasNumbers( source )).toBe( true )
    })
    
    test('empty string to be false', ()=> {
        const source = ''
        expect( hasNumbers( source )).toBe( false )
    })
})


describe('Return strings with numbers', ()=> {
    test("['Hl24lk', 'Fake', 'It is HUGE', 'W1dl4'] to return ['Hl24lk', 'W1dl4']", ()=>{
    
        const source = ['Hl24lk', 'Fake', 'It is HUGE', 'W1dl4']
        const expected = ['Hl24lk', 'W1dl4']
    
        expect( numInStr( source ) ).toEqual( expected )
    })
    
    test("['OPjsd4', '135', '', 'Kosk'] to return ['OPjsd4', '135']", ()=>{
        
        const source = ['OPjsd4', '135', '', 'Kosk']
        const expected = ['OPjsd4', '135']
    
        expect( numInStr( source ) ).toEqual( expected )
    })
})

