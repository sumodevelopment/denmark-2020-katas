export function hasNumbers( inputString ) {
    return [...inputString].some(char => parseInt(char))
}

export function numInStr(inputItems) {
    return inputItems.filter(hasNumbers)
}