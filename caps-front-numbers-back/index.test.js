import { explode, capsToFront, numToBack, reorder } from './'

/**
 * @deprecated
 */
describe('UNIT: String to array', () => {

    test('split hEll2O to [h,E,l,l,2,O]', ()=> {
        const source = 'hEll2O'
        const expected = ['h', 'E', 'l', 'l', '2', 'O']
        expect( explode( source ) ).toEqual( expected )
    })

    test('split WoRd6SareFun to [W,o,R,d,6,S,a,r,e,F,u,n,]', ()=> {
        const source = 'WoRd6SareFun'
        const expected = ['W','o','R','d','6','S','a','r','e','F','u','n']
        expect( explode( source ) ).toEqual( expected )
    })

})

describe('UNIT: Caps to front', ()=> {

    test('split hEll2O to [E,O,h,l,l,2]', ()=> {
        const source = 'hEll2O'
        const expected = ['E', 'O', 'h', 'l', 'l', '2']
        expect( capsToFront( source ) ).toEqual( expected )
    })

    test('split WoRd6SareFun to [W,R,S,F,o,d,6,a,r,e,u,n,]', ()=> {
        const source = 'WoRd6SareFun'
        const expected = ['W','R','S','F','o','d','6','a','r','e','u','n']
        expect( capsToFront( source ) ).toEqual( expected )
    })
})

describe('UNIT: Num to Back', ()=> {

    test("Move nums to back ['E', 'O', 'h', 'l', 'l', '2'] to [E,O,h,l,l,2]", ()=> {
        const source = ['E', 'O', 'h', 'l', 'l', '2']
        const expected = ['E', 'O', 'h', 'l', 'l', '2']
        expect( numToBack( source ) ).toEqual( expected )
    })

    test("split ['W','R','S','F','o','d','6','a','r','e','u','n'] to [W,R,S,F,o,d,a,r,e,u,n,'6']", ()=> {
        const source = ['W','R','S','F','o','d','6','a','r','e','u','n']
        const expected = ['W','R','S','F','o','d','a','r','e','u','n','6']
        expect( numToBack( source ) ).toEqual( expected )
    })
})

describe('INT: Reorder Word', ()=> {
    test('Order wIldStu2Ff to ISFwldtuf2', ()=> {
        expect( reorder( 'wIldStu2Ff' ) ).toBe(  'ISFwldtuf2' )
    })
})