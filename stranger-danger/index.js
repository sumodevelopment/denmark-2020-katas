const THRESHOLD_ACQUAINTANCE = 3
const THRESHOLD_FRIEND = 5

export function noStrangers(sentence) {
    const words = removeSpecialCharacters(sentence).split(' ')
    //•A word encountered for the first time is a stranger. 
    //•A word encountered thrice becomes an acquaintance. 
    //•A word encountered 5 times becomes a friend.
}

export function removeSpecialCharacters(string) {
    return string.replace(/[^a-zA-Z' ]/g, '')
}