import { noStrangers, removeSpecialCharacters } from './'

test('remove special chars See Spot\' run,', () => {

    const source = "See Spot' run,!#"
    const expected = "See Spot' run"

    expect( removeSpecialCharacters( source ) ).toBe( expected )

})

test('', () => {
    const source = "See Spot' run, See Spot jump. Spot likes jumping. See Spot fly."
    
    const expected = [
        ["spot", "see"],
        []
    ]
    //expect(noStrangers(source)).toEqual(expected)
})