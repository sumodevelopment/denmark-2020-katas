import { hiddenAnagram, toCharArray } from './'

test('toCharArrray Clint Eastwood -> [c,l,i,n,t,e,a,s,t,w,o,o,d]', () => {

    const source = "Clint Eastwood"
    const expected = ['c', 'l', 'i', 'n', 't', 'e', 'a', 's', 't', 'w', 'o', 'o', 'd']

    expect(toCharArray(source)).toEqual(expected)

})

test('toCharArrray "An old, west action hero actor!" -> ["a", "n", "o","l","d","w","e","s","t","a","c","t","i","o","n","h","e","r","o","a","c","t","o","r"]', () => {

    const source = "An old west action hero actor!"
    const expected = ["a", "n", "o", "l", "d", "w", "e", "s", "t", "a", "c", "t", "i", "o", "n", "h", "e", "r", "o", "a", "c", "t", "o", "r"]

    expect(toCharArray(source)).toEqual(expected)

})

test('"An old west action hero actor", "Clint Eastwood" -> noldwestactio', () => {
    const sentence = "An old west action hero actor"
    const anagramToFind = "Clint Eastwood"
    expect(hiddenAnagram(sentence, anagramToFind)).toBe("noldwestactio")
})